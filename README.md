# 🛡️ Raid Viking: En Route pour le Valhalla 🛡️

## Bienvenue, fiers guerriers des mers!

Après une longue attente, la saison des tempêtes s'est dissipée, révélant l'horizon tant convoité. 

Les dieux ont enfin ouvert les portes des mers, annonçant le début d'une saison épique de conquêtes et d'aventures. 
Il est temps de hisser les voiles, de réunir vos équipages, et de prendre le large à bord de vos drakkars. 

Votre mission? Naviguer à travers les mers tumultueuses, orchestrer des raids audacieux, et amasser des trésors inestimables. 
Mais soyez avertis, vous n'êtes pas les seuls à convoiter la gloire et la richesse. D'autres clans, tout aussi déterminés, se lancent dans cette quête périlleuse.

## Votre Défi: Obtenir votre siège à la table d'Odin

Pour gagner votre place auprès du Père de Tout, vous devrez non seulement prouver votre valeur en mer, mais aussi maîtriser l'art de la stratégie et de la planification. 
Chaque raid sera une saga en soi, où chaque décision comptera et influencera le dénouement de votre aventure.

## Votre Armurerie de Développeur:
Au cœur de votre drakkar, une armurerie pas comme les autres: **le Docker Compose des Dieux.**

Ce n'est pas seulement par la force des bras que vous naviguerez vers la gloire, mais également grâce à votre maîtrise de l'armurerie divine, un ensemble de services orchestrés par Docker Compose. Chaque service est une bénédiction des dieux, vous guidant à travers les périls et vers les richesses infinies.

**Temple de Thor**: Le prêtre de Thor lance des messages divins à travers les vastes océans, votre premier signe pour commencer le pillage. Ce service vous transmet les signes et augures, indiquant quand et où frapper pour un raid fructueux.

**Temple de Njord**: Avant de lever l'ancre, assurez-vous de gagner la faveur de Njord. Ce service exige des offrandes pour s'assurer que vos compagnons vous suivent et que les mers vous soient favorables. Chaque offrande renforce votre flotte, augmentant vos chances de succès dans les eaux traîtresses.

**Village**: Arrivés sur les lieux de votre entreprise, ce service est crucial pour maximiser le butin. Il représente la planification et l'exécution de votre raid, assurant que vous repartiez avec les coffres remplis à ras bord.

**Temple de Herja**: Après le pillage, présentez votre butin à la Valkyrie Herja. Ce service vous permet de gagner la faveur de la Valkyrie, garantissant que votre raid soit couronné de succès.

**Temple d'Odin**: Tout au long de votre saga, n'oubliez jamais d'honorer Odin. Ce service veille à ce que chaque acte de bravoure, chaque choix stratégique soit un hommage au Père de Tout. Les bénédictions d'Odin sont essentielles pour votre quête du trône.

> Vous trouverez dans le dossier `specs/rest` les spécifications des services fournis pour votre quête

## Prêts à Embarquer?

Assemblez votre équipage, affûtez vos haches, et préparez-vous pour une aventure où stratégie, courage, et ingéniosité détermineront votre destin. 
Êtes-vous prêt à écrire votre saga et à inscrire vos noms sur les murs du Valhalla? Alors hissez les voiles, et que les dieux vous guident vers la gloire éternelle !

## Explications

Le docker-compose des Dieux vous fournit tous les services dont vous aurez besoin pour mener à bien votre quête.

Pour mener à bien un raid, vous devrez intégrer dans votre drakkar les services suivants:

1. créer un consumer asynchrone, branché sur la `voix de Thor` (topic kafka) : quand vous entendrez Thor grondez, c'est le moment de partir en raid.
   - Pour cela, vous devrez offrir des sacrifices `au temple de Njord`, pour que le dieu des Mers et des Vents souffle dans vos voiles. (appel rest sur /offerings_to_njord). 
   - Si le dieu Njord est satisfait, vous pourrez alors lancer votre `Drakkar` sur la mer, poussé par les `vents de Njords` (publication sur le topic kafka)
     - Le message à publier devra avoir le format suivant : 
     ```json
     {
        "raidId": "string",
        "clanId": "string"
     }
     ```
   - Si le dieu Njord n'est pas satisfait, ce raid sera un échec car vos vaillants vikings refuseront de prendre la mer.
   - Quel que soit le résultat, vous devrez envoyer un `Corbeau` au `temple d'Odin` pour lui faire part de votre tentative de raid, de vos succès, de vos échecs et de son issue.
     - le message à publier devra avoir le format suivant :
     ```json
     {
        "raidId": "string",
        "clanId": "string",
         "njordAnswer": "NjordAccepted" | "NjordRejected"
     }
     ```
   - Le prêtre d'Odin devra alors marquer dans `le grand livre d'Odin` le résultat de votre raid. (appel rest sur /write_in_the_book_of_destiny)
   - Attention ! Nul ne ment au Père de Tout. Si vous tentez de lui cacher la vérité, il maudira votre nom et ignorera votre raid.

2. créer un consumer asynchrone, branché sur les `vents de Njords` (topic kafka) : quand  les vents de Njord souffleront dans vos voiles, vous atteindrez votre proie et vous livrerez au pillage de ses richesses.
   - Bravo, navigateurs intrépides ! Vous avez atteint les rivages d'un village à piller. Vous devez à présent pillez les richesses qui s'y trouvent. (appel rest sur /plunder)
   - Si votre pillage est couronné de succès, vous pourrez alors `présentez votre butin à la Valkyrie Herja` (publication d'un message sur le topic`chevauchée d'Herja`), pour qu'elle vous accorde la gloire et les honneurs.
      - Le message à publier devra avoir le format suivant :
     ```json
     {
        "raidId": "string",
        "clanId": "string"
     }
     ```
   - Si vous échouez, par manque de butin (réponse 404), ou parce que vous avez été repoussé (réponse 409), le raid est un échec et vous pouvez rentrer chez vous, les mains vides et la tête basse.
   - Quel que soit le résultat, vous devrez envoyer un `Corbeau` au `temple d'Odin` pour lui faire part de votre tentative de raid, de vos succès, de vos échecs et de son issue.
     - Le message à publier devra avoir le format suivant :
     ```json
     {
        "raidId": "string",
        "clanId": "string",
        "villageOutcome": "SuccessWithSpoils" | "SuccessWithoutSpoils" | "RepelledToTheSea"
     }
     ```

3. créer un consumer asynchrone, branché sur la `chevauchée d'Herja` (topic kafka) : sur le chemin du retour, vous vous arrêtez au `temple d'Herja` 
   - Vous présenterez à la valkyrie des pillages le fruit de vos conquêtes (appel rest sur /present_spoils_to_herja)
   - Si votre butin est jugé suffisant, vous serez honoré et votre raid sera un succès, et vous aurez fait un pas de plus vers la gloire éternelle du Valhalla. 
   - Si votre butin est jugé insuffisant (erreur http 417), votre raid sera un échec, et Herja fera de vous la risée de tous vos ancêtres.
   - Quel que soit le résultat, vous devrez envoyer un `Corbeau` au `temple d'Odin` pour lui faire part de votre tentative de raid, de vos succès et de vos échecs et de son issue.
        - le message à publier devra avoir le format suivant :
        ```json
        {
            "raidId": "string",
            "clanId": "string",
            "herjaAnswer": "HerjaAccepted" | "HerjaRejected"
        }
        ```
4. Créez un consumer asynchrone, branché sur les `corbeaux d'Odin ` (topic kafka) :
   - A chaque étape, vous avez du envoyer un corbeau au prêtre d'Odin pour l'informer du résultat de l'étape
   - Lorsque le prêtre sera informé de votre retour et succes ou de l'echec de votre raid, il marquera dans `le grand livre d'Odin` le résultat de votre raid. (appel rest sur /write_in_the_book_of_destiny)
   - Attention ! Nul ne ment au Père de Tout. Si vous tentez de lui cacher la vérité, ou de lui mentir, il maudira votre nom et infligera sa colère sur votre clan.

> Vous trouverez dans le dossier `specs/async` les spécifications des services à intégrer dans votre drakkar.

## Fin de la saison des raids

A la fin de la saison des raids, le clan qui aura marqué le plus de raid dans le livre d'Odin, et qui aura rapporté fidèlement à Odin les résultats de chacun de ces raids, 
sera déclaré vainqueur et le clan aura gagné sa place à la table d'Odin, dans les halls dorés du Valhalla.

## Décompte des points

> - Chaque raid réussi et fidèlement rapporté à Odin rapporte 2 points 
> 
> - Chaque raid échoué et fidèlement rapporté à Odin rapporte 1 point
> 
> - Chaque échec à rapporter fidèlement à Odin le résultat et les étapes d'un raid fait perdre 1 point
> 
> - Chaque raid rapporté à Odin et n'ayant jamais eu lieu fait perdre 1 points

## Préparer la saison des raids

Commencez par vous logger avec le registry docker du projet, pour pouvoir pull les images nécessaires à la réalisation de votre quête.

```bash
docker login -u exaltemps-it --password-stdin <<< gldt-YEZfn-qzs5j5gddZWdvs registry.gitlab.com/exalt-it-dojo/events/exaltemps/challenges/viking-raid/raid-services
```

Lancez ensuite le docker-compose des Dieux, pour obtenir les services nécessaires à votre quête.

```bash
docker-compose up
```

![Processus de Raid](./specs/exalt-raid.svg)

A vous de jouer ! Que les dieux vous guident vers la gloire éternelle du Valhalla !