#!/bin/bash

echo "Creating Kafka topics... $TOPICS"

KT="/opt/bitnami/kafka/bin/kafka-topics.sh"

"$KT" --bootstrap-server localhost:9092 --list

echo "$TOPICS"
IFS=";"
read -ra TOPICS_LIST <<< "$TOPICS"

for topic in "${TOPICS_LIST[@]}"
do
  echo "Creating topic $topic"
  IFS=":"
  read -ra topic_config <<< "$topic"
  echo $topic_config
  "$KT" --create --topic "${topic_config[0]}" --partitions "${topic_config[1]}" --replication-factor "${topic_config[2]}" --bootstrap-server localhost:9092
done
